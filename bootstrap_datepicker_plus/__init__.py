"""This module contains the widget classes."""

from bootstrap_datepicker_plus._base import BasePickerInput


__all__ = (
    'DatePickerInput',
    'TimePickerInput',
    'DateTimePickerInput',
    'MonthPickerInput',
    'YearPickerInput',
)


class DatePickerInput(BasePickerInput):
    """
    Widget to display a Date-Picker Calendar on a DateField property.

    Args:
        - attrs (dict): HTML attributes of rendered HTML input
        - format (string): Python DateTime format eg. "%Y-%m-%d"
        - options (dict): Options to customize the widget, see README
    """

    picker_type = 'DATE'
    format = '%d.%m.%Y'
    format_key = 'DATE_INPUT_FORMATS'


class TimePickerInput(BasePickerInput):
    """
    Widget to display a Time-Picker Calendar on a TimeField property.

    Args:
        - attrs (dict): HTML attributes of rendered HTML input
        - format (string): Python DateTime format eg. "%Y-%m-%d"
        - options (dict): Options to customize the widget, see README
    """

    picker_type = 'TIME'
    format = '%H:%M'
    format_key = 'TIME_INPUT_FORMATS'
    template_name = 'bootstrap_datepicker_plus/time-picker.html'


class DateTimePickerInput(BasePickerInput):
    """
    Widget to display a DateTime-Picker Calendar on a DateTimeField property.

    Args:
        - attrs (dict): HTML attributes of rendered HTML input
        - format (string): Python DateTime format eg. "%Y-%m-%d"
        - options (dict): Options to customize the widget, see README
    """

    picker_type = 'DATETIME'
    format = '%d.%m.%Y %H:%M'
    format_key = 'DATETIME_INPUT_FORMATS'


class MonthPickerInput(BasePickerInput):
    """
    Widget to display a Month-Picker Calendar on a DateField property.

    Args:
        - attrs (dict): HTML attributes of rendered HTML input
        - format (string): Python DateTime format eg. "%Y-%m-%d"
        - options (dict): Options to customize the widget, see README
    """

    picker_type = 'MONTH'
    format = '01.%m.%Y'
    format_key = 'DATE_INPUT_FORMATS'


class YearPickerInput(BasePickerInput):
    """
    Widget to display a Year-Picker Calendar on a DateField property.

    Args:
        - attrs (dict): HTML attributes of rendered HTML input
        - format (string): Python DateTime format eg. "%Y-%m-%d"
        - options (dict): Options to customize the widget, see README
    """

    picker_type = 'YEAR'
    format = '01.01.%Y'
    format_key = 'DATE_INPUT_FORMATS'

    def _link_to(self, linked_picker):
        """Customize the options when linked with other date-time input"""
        yformat = self.config['options']['format'].replace('01.01', '31.12')
        self.config['options']['format'] = yformat
